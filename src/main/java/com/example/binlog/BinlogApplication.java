package com.example.binlog;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.binlog.dao")
public class BinlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BinlogApplication.class, args);
    }

}
